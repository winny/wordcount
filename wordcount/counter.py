from typing import Dict
from collections import defaultdict
from operator import itemgetter
import re


class WordCounter(object):
    """Utility class to count words of file-like objects."""

    def __init__(self):
        """Initialize"""
        self._frequencies = defaultdict(lambda: 0)

    def scan(self, file):
        """Scan a file for words"""
        for line in file:
            for word in re.split(r"[^A-Za-z]+", line):
                if not word:
                    continue  # Empty line
                self._frequencies[word.lower()] += 1

    def report(self) -> str:
        """Generate a report on frequencies."""
        offset = max(len(word) for word in self._frequencies.keys())

        def make_line(word, count):
            """Generate a line (sans the newline) for a statistic."""
            return "".join(
                [
                    word,
                    " " * (offset - len(word)),  # Padding
                    " | ",  # Separator
                    str(count),
                ]
            )

        return "\n".join(
            make_line(word, count)
            for word, count in sorted(
                self._frequencies.items(), key=itemgetter(0)
            )
        )

    @property
    def frequencies(self) -> Dict[str, int]:
        """Give back a copy of the internal frequencies counter."""
        return dict(self._frequencies)
