"""python -m unittest"""


from io import StringIO
from .counter import WordCounter
from unittest import TestCase


class TestWordCountClass(TestCase):
    """Test the word count (class)."""

    def setUp(self):
        """Set up"""
        self.counter = WordCounter()

    def test_empty(self):
        """Verify sanity."""
        self.assertEqual(self.counter.frequencies, {})

    def test_scan(self):
        """Ensure the frequencies make sense."""
        self.counter.scan(
            StringIO(
                """
I can see$$#$the%'
blue_ _shark in; the. blue blue water!" """
            )
        )
        self.assertEqual(
            self.counter.frequencies,
            {
                "blue": 3,
                "shark": 1,
                "the": 2,
                "i": 1,
                "can": 1,
                "see": 1,
                "in": 1,
                "water": 1,
            },
        )

    def test_report(self):
        """Check that words have correct frequency & order in the report."""
        self.counter.scan(
            StringIO(
                """
def main():
    # Show the hello world
    print('hello world')
"""
            )
        )
        expected_counts = [
            ("def", 1),
            ("hello", 2),
            ("main", 1),
            ("print", 1),
            ("show", 1),
            ("the", 1),
            ("world", 2),
        ]
        for line, (word, count) in zip(
            self.counter.report().splitlines(), expected_counts
        ):
            self.assertIn(word, line)
            self.assertIn(str(count), line)
