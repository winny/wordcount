import argparse
from .counter import WordCounter


def make_parser():
    p = argparse.ArgumentParser(description="Count words in text documents")
    p.add_argument(
        "files",
        metavar="FILE",
        nargs=3,
        type=argparse.FileType("r", encoding="UTF-8"),
    )
    return p


def main():
    parser = make_parser()
    args = parser.parse_args()
    counter = WordCounter()
    for f in args.files:
        counter.scan(f)
    print(counter.report())
