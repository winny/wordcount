from setuptools import setup


with open("README.rst") as f:
    readme = f.read()


setup(
    name="wordcount",
    version="0.0.1",
    description="Count words in files.",
    long_description=readme,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.9",
        "Topic :: Utilities",
    ],
    url="https://gitlab.com/winny/wordcount",
    author="Winston Weinert",
    author_email="winston@ml1.net",
    license="MIT",
    packages=["wordcount"],
    install_requires=[],
    entry_points={"console_scripts": ["wordcount=wordcount.cli:main"]},
)
