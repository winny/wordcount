#######################
wordcount
#######################

Count words in files.

**********
Installing
**********

Install via via ``setup.py``::

  python setup.py install

*******
Running
*******

Try it out via::

  python -m wordcount wordcount/cli.py \
      wordcount/counter.py \
      wordcount/__init__.py

One can use ``wordcount wordcount/{cli,counter,__init__}.py`` if one installs
it.

**Note:** this takes 3 arguments as per the requirements.

************
Contributing
************

Please add and run tests.::

  python -m unittest

Set up pre-commit.::

  pre-commit install

*******
License
*******

MIT/X (c) Winston Weinert, see ``LICENSE.md`` for full text.
